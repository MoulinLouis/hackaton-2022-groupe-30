<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311085855 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE vivo DROP CONSTRAINT fk_8c7e724a1dfbcc46');
        $this->addSql('DROP INDEX idx_8c7e724a1dfbcc46');
        $this->addSql('ALTER TABLE vivo RENAME COLUMN rapport_id TO data_id');
        $this->addSql('ALTER TABLE vivo ADD CONSTRAINT FK_8C7E724A37F5A13C FOREIGN KEY (data_id) REFERENCES data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_8C7E724A37F5A13C ON vivo (data_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE vivo DROP CONSTRAINT FK_8C7E724A37F5A13C');
        $this->addSql('DROP INDEX IDX_8C7E724A37F5A13C');
        $this->addSql('ALTER TABLE vivo RENAME COLUMN data_id TO rapport_id');
        $this->addSql('ALTER TABLE vivo ADD CONSTRAINT fk_8c7e724a1dfbcc46 FOREIGN KEY (rapport_id) REFERENCES data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_8c7e724a1dfbcc46 ON vivo (rapport_id)');
    }
}
