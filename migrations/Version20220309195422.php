<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309195422 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE question_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE response_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE tubo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE vivo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE question (id INT NOT NULL, rapport_id INT DEFAULT NULL, question VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B6F7494E1DFBCC46 ON question (rapport_id)');
        $this->addSql('CREATE TABLE response (id INT NOT NULL, question_id INT DEFAULT NULL, response VARCHAR(255) DEFAULT NULL, nb_answer INT DEFAULT NULL, temperature INT DEFAULT NULL, uv INT DEFAULT NULL, olevel INT DEFAULT NULL, weather_condition INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3E7B0BFB1E27F6BF ON response (question_id)');
        $this->addSql('CREATE TABLE tubo (id INT NOT NULL, rapport_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, score DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1DE537801DFBCC46 ON tubo (rapport_id)');
        $this->addSql('CREATE TABLE vivo (id INT NOT NULL, rapport_id INT DEFAULT NULL, t0 DOUBLE PRECISION DEFAULT NULL, t_imemediat DOUBLE PRECISION DEFAULT NULL, t7 DOUBLE PRECISION DEFAULT NULL, t14 DOUBLE PRECISION DEFAULT NULL, zone INT DEFAULT NULL, score_skin INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8C7E724A1DFBCC46 ON vivo (rapport_id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E1DFBCC46 FOREIGN KEY (rapport_id) REFERENCES data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE response ADD CONSTRAINT FK_3E7B0BFB1E27F6BF FOREIGN KEY (question_id) REFERENCES question (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tubo ADD CONSTRAINT FK_1DE537801DFBCC46 FOREIGN KEY (rapport_id) REFERENCES data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE vivo ADD CONSTRAINT FK_8C7E724A1DFBCC46 FOREIGN KEY (rapport_id) REFERENCES data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE data DROP content');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE response DROP CONSTRAINT FK_3E7B0BFB1E27F6BF');
        $this->addSql('DROP SEQUENCE question_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE response_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE tubo_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE vivo_id_seq CASCADE');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE response');
        $this->addSql('DROP TABLE tubo');
        $this->addSql('DROP TABLE vivo');
        $this->addSql('ALTER TABLE data ADD content JSON DEFAULT NULL');
    }
}
