<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220310100801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tubo DROP CONSTRAINT fk_1de537801dfbcc46');
        $this->addSql('DROP INDEX idx_1de537801dfbcc46');
        $this->addSql('ALTER TABLE tubo RENAME COLUMN rapport_id TO data_id');
        $this->addSql('ALTER TABLE tubo ADD CONSTRAINT FK_1DE5378037F5A13C FOREIGN KEY (data_id) REFERENCES data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1DE5378037F5A13C ON tubo (data_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE tubo DROP CONSTRAINT FK_1DE5378037F5A13C');
        $this->addSql('DROP INDEX IDX_1DE5378037F5A13C');
        $this->addSql('ALTER TABLE tubo RENAME COLUMN data_id TO rapport_id');
        $this->addSql('ALTER TABLE tubo ADD CONSTRAINT fk_1de537801dfbcc46 FOREIGN KEY (rapport_id) REFERENCES data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_1de537801dfbcc46 ON tubo (rapport_id)');
    }
}
