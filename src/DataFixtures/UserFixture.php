<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker\Factory;

class UserFixture extends Fixture
{
    public const PASSWORD = 'password123';
    public const USER_ENTRIES = 5;

    /**
     * @var \Faker\Generator $faker
     */
    private \Faker\Generator $faker;

    /**
     * @var UserPasswordHasherInterface $encoder
     */
    public function __construct(private UserPasswordHasherInterface $encoder)
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        // Administrator user
        $user = new User();
        $user->setName("Administrator");
        $user->setEmail("admin@admin");
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword($this->encoder->hashPassword(
            $user,
            self::PASSWORD
        ));
        $manager->persist($user);

        // Ben Dover user
        $user = new User();
        $user->setName("Ben Dover");
        $user->setEmail("bdover@gmail.com");
        $user->setRoles(['ROLE_USER']);
        $user->setPassword($this->encoder->hashPassword(
            $user,
            self::PASSWORD
        ));
        $manager->persist($user);

        // x random users
        for ($i = 0; $i < self::USER_ENTRIES; $i++) {
            $user = new User();
            $user->setName($this->faker->name);
            $user->setEmail($this->faker->email);
            $user->setRoles(['ROLE_USER']);
            $user->setPassword($this->encoder->hashPassword(
                $user,
                self::PASSWORD
            ));
            $manager->persist($user);
        }

        $manager->flush();
    }
}
