<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Company name :',
                'label_attr' => ['class' => 'form-label switzer light'],
                'attr' => [
                    'class' => 'form-control switzer',
                    'placeholder' => 'My Company'
                ]])
            ->add('email', TextType::class, [
                'label' => 'Email :',
                'label_attr' => ['class' => 'form-label switzer light'],
                'attr' => [
                    'class' => 'form-control switzer',
                    'placeholder' => 'john.doe@example.com'
                ]])
            ->add('save', SubmitType::class, [
                'label' => 'Save',
                'attr' => [
                    'class' => 'btn',
                    'style' => 'background-color: #197496; color: white;'
                ],
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
