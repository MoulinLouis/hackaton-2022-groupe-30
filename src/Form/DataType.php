<?php

namespace App\Form;

use App\Entity\Data;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('file', FileType::class, [
                'label' => 'Data file (CSV or XLSX)',
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control switzer',
                ]])
            ->add('save', SubmitType::class, [
                'label' => 'Save',
                'attr' => [
                    'class' => 'btn',
                    'style' => 'background-color: #197496; color: white;'
                ],
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {

    }
}
