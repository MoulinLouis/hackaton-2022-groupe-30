<?php
namespace App\Utils;

class ExcelHelper
{
    public static function transformSheetToArray($worksheet, $columnLimit): array
    {
        $i = 0;
        $table = [];

        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $j=0;
            foreach ($cellIterator as $key => $cell) {
                if($i != 0 & $j < $columnLimit) {
                    $table[$i][$j] = $cell->getValue();
                }
                $j++;
            }
            $i++;
        }
        return $table;
    }
}