<?php

namespace App\Entity;

use App\Repository\DataRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DataRepository::class)]
class Data
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $name;

    #[ORM\ManyToOne(targetEntity: Template::class, inversedBy: 'data')]
    private $template;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'data')]
    private $users;

    #[ORM\OneToMany(mappedBy: 'data', targetEntity: Tubo::class)]
    private $tubos;

    #[ORM\OneToMany(mappedBy: 'data', targetEntity: Vivo::class)]
    private $vivos;

    #[ORM\OneToMany(mappedBy: 'data', targetEntity: Question::class)]
    private $questions;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->tubos = new ArrayCollection();
        $this->vivos = new ArrayCollection();
        $this->questions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTemplate(): ?Template
    {
        return $this->template;
    }

    public function setTemplate(?Template $template): self
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * @return Collection<int, Tubo>
     */
    public function getTubos(): Collection
    {
        return $this->tubos;
    }

    public function addTubo(Tubo $tubo): self
    {
        if (!$this->tubos->contains($tubo)) {
            $this->tubos[] = $tubo;
            $tubo->setData($this);
        }

        return $this;
    }

    public function removeTubo(Tubo $tubo): self
    {
        if ($this->tubos->removeElement($tubo)) {
            // set the owning side to null (unless already changed)
            if ($tubo->getData() === $this) {
                $tubo->setData(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Vivo>
     */
    public function getVivos(): Collection
    {
        return $this->vivos;
    }

    public function addVivo(Vivo $vivo): self
    {
        if (!$this->vivos->contains($vivo)) {
            $this->vivos[] = $vivo;
            $vivo->setData($this);
        }

        return $this;
    }

    public function removeVivo(Vivo $vivo): self
    {
        if ($this->vivos->removeElement($vivo)) {
            // set the owning side to null (unless already changed)
            if ($vivo->getData() === $this) {
                $vivo->setData(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Question>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setData($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): self
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getData() === $this) {
                $question->setData(null);
            }
        }

        return $this;
    }
}
