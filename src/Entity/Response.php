<?php

namespace App\Entity;

use App\Repository\ResponseRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ResponseRepository::class)]
class Response
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $response;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $nbAnswer;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $temperature;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $uv;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $olevel;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $weather_condition;

    #[ORM\ManyToOne(targetEntity: Question::class, inversedBy: 'responses')]
    private $question;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function setResponse(?string $response): self
    {
        $this->response = $response;

        return $this;
    }

    public function getNbAnswer(): ?int
    {
        return $this->nbAnswer;
    }

    public function setNbAnswer(?int $nbAnswer): self
    {
        $this->nbAnswer = $nbAnswer;

        return $this;
    }

    public function getTemperature(): ?int
    {
        return $this->temperature;
    }

    public function setTemperature(?int $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getUv(): ?int
    {
        return $this->uv;
    }

    public function setUv(?int $uv): self
    {
        $this->uv = $uv;

        return $this;
    }

    public function getOlevel(): ?int
    {
        return $this->olevel;
    }

    public function setOlevel(?int $olevel): self
    {
        $this->olevel = $olevel;

        return $this;
    }

    public function getWeatherCondition(): ?int
    {
        return $this->weather_condition;
    }

    public function setWeatherCondition(?int $weather_condition): self
    {
        $this->weather_condition = $weather_condition;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }
}
