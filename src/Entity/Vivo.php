<?php

namespace App\Entity;

use App\Repository\VivoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VivoRepository::class)]
class Vivo
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'float', nullable: true)]
    private $t0;

    #[ORM\Column(type: 'float', nullable: true)]
    private $tImemediat;

    #[ORM\Column(type: 'float', nullable: true)]
    private $t7;

    #[ORM\Column(type: 'float', nullable: true)]
    private $t14;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $zone;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $score_skin;

    #[ORM\ManyToOne(targetEntity: Data::class, inversedBy: 'vivos')]
    private $data;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getT0(): ?float
    {
        return $this->t0;
    }

    public function setT0(?float $t0): self
    {
        $this->t0 = $t0;

        return $this;
    }

    public function getTImemediat(): ?float
    {
        return $this->tImemediat;
    }

    public function setTImemediat(?float $tImemediat): self
    {
        $this->tImemediat = $tImemediat;

        return $this;
    }

    public function getT7(): ?float
    {
        return $this->t7;
    }

    public function setT7(?float $t7): self
    {
        $this->t7 = $t7;

        return $this;
    }

    public function getT14(): ?float
    {
        return $this->t14;
    }

    public function setT14(?float $t14): self
    {
        $this->t14 = $t14;

        return $this;
    }

    public function getZone(): ?int
    {
        return $this->zone;
    }

    public function setZone(?int $zone): self
    {
        $this->zone = $zone;

        return $this;
    }

    public function getScoreSkin(): ?int
    {
        return $this->score_skin;
    }

    public function setScoreSkin(?int $score_skin): self
    {
        $this->score_skin = $score_skin;

        return $this;
    }

    public function getData(): ?Data
    {
        return $this->data;
    }

    public function setData(?Data $data): self
    {
        $this->data = $data;

        return $this;
    }
}
