<?php

namespace App\Controller\Application;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractApplicationController
{
    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        return $this->render('application/index.html.twig', [

        ]);
    }
}
