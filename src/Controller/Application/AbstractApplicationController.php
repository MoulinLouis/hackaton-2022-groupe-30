<?php

namespace App\Controller\Application;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class AbstractApplicationController extends AbstractController
{
    protected const PAGINATION_LIMIT = 16;
}
