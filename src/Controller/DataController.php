<?php

namespace App\Controller;

use App\Entity\Data;
use App\Entity\Tubo;
use App\Entity\User;
use App\Entity\Vivo;
use App\Form\DataType;
use App\Form\DataUserType;
use App\Utils\ExcelHelper;
use Doctrine\Persistence\ManagerRegistry;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Vtiful\Kernel\Excel;

class DataController extends AbstractController
{

    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    #[Route('/datas', name: 'app_data_user')]
    public function user_datas(): Response
    {
        $datas = $this->getUser()->getData();
        return $this->render('data/index.html.twig', [
            'datas' => $datas,
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/admin/datas', name: 'app_data_admin')]
    public function admin_users(): Response
    {
        $datas = $this->doctrine->getRepository(Data::class)->findAll();
        return $this->render('data/index.html.twig', [
            'datas' => $datas,
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/data/add', name: 'add_data')]
    public function add(Request $request, ManagerRegistry $doctrine): Response
    {

        $form = $this->createForm(DataType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get("file")->getData();
            $this->getData($file);
        }

        return $this->renderForm('data/add.html.twig', [
            'form_add' => $form,
        ]);
    }

    public function getData($file) {
        $spreadsheet = IOFactory::load($file->getPathname());
        $em = $this->doctrine->getManager();

        $data = new Data();
        $data->setName($file->getClientOriginalName());

        $em->persist($data);
        $em->flush();

        $this->setTuboData($spreadsheet->getSheet(0), $data->getId());

        $this->setVivoData($spreadsheet->getSheet(1), $data->getId());

        return $this->redirectToRoute('app_data_admin');
    }

    public function setVivoData($worksheet, $data_id) {

        $data = $this->doctrine->getRepository(Data::class)->find($data_id);

        $sheetArray = ExcelHelper::transformSheetToArray($worksheet, 6);

        $average = $this->getVivoAverage($sheetArray, 1, 1, 1);

        $this->setVivoSpecificZoneScore($sheetArray, 1, 1, $data);
        $this->setVivoSpecificZoneScore($sheetArray, 1, 2, $data);
        $this->setVivoSpecificZoneScore($sheetArray, 1, 3, $data);

        $this->setVivoSpecificZoneScore($sheetArray, 2, 1, $data);
        $this->setVivoSpecificZoneScore($sheetArray, 2, 2, $data);
        $this->setVivoSpecificZoneScore($sheetArray, 2, 3, $data);

    }

    public function setVivoSpecificZoneScore($sheetArray, $zone, $skin, $data) {
        $em = $this->doctrine->getManager();

        $vivo = new Vivo();

        $vivo->setZone($zone);
        $vivo->setScoreSkin($skin);
        $vivo->setT0($this->getVivoAverage($sheetArray, $zone, $skin, 1));
        $vivo->setTImemediat($this->getVivoAverage($sheetArray, $zone, $skin, 2));
        $vivo->setT7($this->getVivoAverage($sheetArray, $zone, $skin, 3));
        $vivo->setT14($this->getVivoAverage($sheetArray, $zone, $skin, 4));

        $data->addVivo($vivo);

        $em->persist($vivo);
        $em->persist($data);

        $em->flush();
    }

    public function getVivoAverage($data, $zone, $skin, $session) {
        $array = [];

        for($i = 1; $i < count($data); $i++) {
            if($data[$i][2] == $zone && $data[$i][3] == $skin && $data[$i][4] == $session) {
                array_push($array, $data[$i][5]);
            }
        }

        $filter = array_filter($array);
        return array_sum($filter) / count($filter); // average
    }

    public function setTuboData($worksheet, $id) {
        $em = $this->doctrine->getManager();

        $data = $this->doctrine->getRepository(Data::class)->find($id);

        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();

            $tubo = new Tubo();
            foreach ($cellIterator as $cell) {
                // $name = $cell->getValue();
                $score = !empty($cell->getValue()) ? $cell->getValue() : null;
                if(!empty($score) && !empty($name)) {
                    $tubo->setName($name);
                    $tubo->setScore((float) $score);
                }
                $name = !empty($cell->getValue()) ? $cell->getValue() : null;
                // $score = $cell->getValue();
            }
            $data->addTubo($tubo);

            $em->persist($tubo);
            $em->flush();
        }

        $em->persist($data);
        $em->flush();
    }

    public function setQuestionData() {

    }

    #[Route('/report/{id}', name: 'generate_report')]
    public function generateChart(int $id, ManagerRegistry $doctrine): Response
    {
        $report = $doctrine->getRepository(Data::class)->find($id) ?? false;
        if (!$report) return $this->redirectToRoute("app_data");

        $vivos = $report->getVivos();

        $zone1 = array_values(array_filter($vivos->map(function ($element) {
            return ($element->getZone() == 1) ? $element : null;
        })->toArray()));

        $zone2 = array_values(array_filter($vivos->map(function ($element) {
            return ($element->getZone() == 2) ? $element : null;
        })->toArray()));

        return $this->renderForm('data/charts.html.twig', [
            "report" => $report,
            "zone1" => $zone1,
            "zone2" => $zone2
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/admin/data/users/{id}', name: 'app_data_users')]
    public function admin_data_users(int $id, ManagerRegistry $doctrine): Response
    {
        $data = $doctrine->getRepository(Data::class)->find($id);

        $users = $data->getUsers();

        return $this->render('data/user/users.html.twig', [
            'users' => $users,
            'data' => $data
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/admin/data/users/remove/{id}/{user}', name: 'app_data_users_remove')]
    public function admin_data_users_remove(int $id, int $user, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();

        $report = $doctrine->getRepository(Data::class)->find($id);
        $user = $doctrine->getRepository(User::class)->find($user);

        $report->removeUser($user);

        $em->persist($report);
        $em->flush();

        return $this->redirectToRoute('app_data_users', ["id" => $id]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/admin/data/users/add/{id}', name: 'app_data_user_add')]
    public function admin_data_user_add(int $id, ManagerRegistry $doctrine, Request $request): Response
    {

        $form = $this->createForm(DataUserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $doctrine->getManager();
            $data = $doctrine->getRepository(Data::class)->find($id);

            $user = $form->get('user')->getData();
            $data->addUser($user);
            $em->persist($data);
            $em->flush();

            return $this->redirectToRoute('app_data_admin');

        }

        return $this->renderForm('data/user/add.html.twig', [
            'form_add' => $form
        ]);
    }

}
