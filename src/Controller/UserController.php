<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\ByteString;

//#[IsGranted("ROLE_ADMIN")]
class UserController extends AbstractController
{
    #[Route('/admin/users', name: 'admin_users')]
    public function admin_users(ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();
        $users = $em->getRepository(User::class)->findAll();
        return $this->render('user/index.html.twig', [
            'users' => $users,
        ]);
    }

    #[Route('/users/add', name: 'admin_users_add')]
    public function admin_users_add(ManagerRegistry $doctrine, Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {

        $form = $this->createForm(UserType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $doctrine->getManager();
            $user = $form->getData();
            $user->setRoles(["ROLE_USER"]);

            $pwd = ByteString::fromRandom(15)->toString();

            $mail = new MailerController();
            $mail->sendEmail($user->getEmail(), $user->getName(), $pwd);

            $user->setPassword($passwordHasher->hashPassword($user, $pwd));

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_users');
        }

        return $this->renderForm('user/add.html.twig', [
            'form_add' => $form,
        ]);

    }

    #[Route('/users/delete/{id}', name: 'admin_users_deletion')]
    public function admin_users_deletion(int $id, ManagerRegistry $doctrine): Response
    {
        $user = $doctrine->getRepository(User::class)
            ->find($id);

        $em = $doctrine->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('admin_users');
    }

    #[Route('/users/edit/{id}', name: 'admin_users_edit')]
    public function admin_users_edit(int $id, ManagerRegistry $doctrine, Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $user = $doctrine->getRepository(User::class)
            ->find($id);

        $form = $this->createForm(UserType::class)->setData($user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $doctrine->getManager();
            $userEdit = $form->getData();

            $user->setName($userEdit->getName());
            $user->setEmail($userEdit->getEmail());
            $user->setPassword($passwordHasher->hashPassword($user, $userEdit->getPassword()));

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_users');
        }

        return $this->renderForm('user/edit.html.twig', [
            'form_edit' => $form,
        ]);

    }
}
