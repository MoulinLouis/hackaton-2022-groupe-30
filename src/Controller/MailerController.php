<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class MailerController extends AbstractController
{
    public function sendEmail($to, $name, $pwd)
    {
        $html = '<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
                  <div style="margin:50px auto;width:70%;padding:20px 0">
                    <div style="border-bottom:1px solid #eee">
                        <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Wired Beauty</a>
                    </div>
                    <p style="font-size:1.1em">Hello ' .
                    $name .
                    '</p>
                    <p>Thank you for joining us ! Here\'s your password : '. $pwd .'</p>
                  </div>
                 </div>';

        $transport = Transport::fromDsn($_SERVER["MAILER_DSN"]);
        $mailer = new Mailer($transport);
        $email = (new Email())->from(new Address('wiredbeauty30@gmail.com', 'Wired Beauty'))
            ->to($to)
            ->priority(Email::PRIORITY_HIGH)
            ->subject($name . ", Welcome to Wired Beauty")
            ->html($html);

        try {
            $mailer->send($email);
            return true;
        } catch (TransportExceptionInterface $e) {
            dd($e);
            return $e;
        }
    }
}
