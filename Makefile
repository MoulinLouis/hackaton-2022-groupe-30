.PHONY: install
install: # Install site
	cp .env.example .env
	docker-compose up -d
	composer install
	npm install
	npm run dev
	docker exec -it php php bin/console doctrine:migration:migrate
	docker exec -it php php bin/console doctrine:fixture:load