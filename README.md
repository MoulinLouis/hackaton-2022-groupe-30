# Hackaton 2022 - Groupe 30

La partie front-office et la partie back-office sont séparées. [L'accès au front](#access-front-office) se fait via des liens webflow, [l'accès au back](#getting-started-back-office) se fait via l'application symfony présente sur ce git.

## Stacks for the back-office (symfony)
- [php 8.0](https://www.php.net/releases/8.0/en.php)
- [Symfony 6](https://symfony.com/releases/6.0)
- [twig](https://twig.symfony.com/) - Moteur de template
- [PostgreSQL 13.4](https://www.postgresql.org/docs/release/13.4/) - Base de données
- [phpPgAdmin](https://fr.wikipedia.org/wiki/PhpPgAdmin) - Interface de gestion du SGBD PostgreSQL
- [webpack encore](https://symfony.com/doc/current/frontend.html) - Manager CSS et JS
- [bootstrap](https://getbootstrap.com/) - Librairie pour le style
- [amChart](https://www.amcharts.com/) - Génération du dataviz
- [pdfMake](http://pdfmake.org/#/) - Export pdf du dataviz

## Stacks for the front-office
- [Webflow](https://www.google.com)
- [Intercom](https://www.google.com)
- [Cookie script](https://www.google.com)
- [Bablic](https://www.google.com)
- [Integromat](https://www.google.com)

## Getting started back-office (symfony)
```
cd 2021-2022-4iw-group-30
make install
```
or run
```
cd 2021-2022-4iw-group-30
cp .env.example .env
docker-compose up -d
composer install
npm install
npm run dev
docker exec -it php php bin/console doctrine:migration:migrate
docker exec -it php php bin/console doctrine:fixture:load
```

## Default users
- [ROLE_ADMIN]
  - admin@admin:password123 
- [ROLE_USER]
  - bdover@gmail.com:password123

## Access front-office
- [Home](https://wired-beauty.webflow.io/)
- [What we do](https://wired-beauty.webflow.io/what-we-do)
- [New device](https://wired-beauty.webflow.io/new-device)
- [Who we are](https://wired-beauty.webflow.io/who-we-are)
- [Job board](https://wired-beauty.webflow.io/job-boards)