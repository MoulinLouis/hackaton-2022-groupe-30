function generateQuestionChart() {
    am5.ready(function(data) {

        var root = am5.Root.new("questionChart");

        root.setThemes([am5themes_Animated.new(root)]);

        var chart = root.container.children.push(
            am5xy.XYChart.new(root, {
                panX: false,
                panY: false,
                wheelX: "panX",
                wheelY: "zoomX",
                layout: root.verticalLayout
            })
        );

        chart.set(
            "scrollbarX",
            am5.Scrollbar.new(root, {
                orientation: "horizontal"
            })
        );

        var xAxis = chart.xAxes.push(
            am5xy.CategoryAxis.new(root, {
                categoryField: "response",
                renderer: am5xy.AxisRendererX.new(root, {}),
            })
        );

        xAxis.data.setAll(data);

        var yAxis = chart.yAxes.push(
            am5xy.ValueAxis.new(root, {
                min: 0,
                extraMax: 0.1,
                renderer: am5xy.AxisRendererY.new(root, {})
            })
        );

        var series1 = chart.series.push(
            am5xy.ColumnSeries.new(root, {
                name: "nbAnswers",
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: "nbAnswers",
                categoryXField: "response"
            })
        );

        series1.columns.template.setAll({
            tooltipY: am5.percent(10),
            templateField: "columnSettings"
        });

        series1.data.setAll(data);

        var series2 = chart.series.push(
            am5xy.LineSeries.new(root, {
                name: "UV",
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: "uv",
                categoryXField: "response"
            })
        );

        series2.strokes.template.setAll({
            strokeWidth: 3,
            templateField: "strokeSettings"
        });


        series2.data.setAll(data);

        series2.bullets.push(function () {
            return am5.Bullet.new(root, {
                sprite: am5.Circle.new(root, {
                    strokeWidth: 3,
                    stroke: series2.get("stroke"),
                    radius: 5,
                    fill: root.interfaceColors.get("background")
                })
            });
        });

        var series3 = chart.series.push(
            am5xy.LineSeries.new(root, {
                name: "temperature",
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: "temperature",
                categoryXField: "response"
            })
        );

        series3.strokes.template.setAll({
            strokeWidth: 3,
            templateField: "strokeSettings"
        });


        series3.data.setAll(data);

        series3.bullets.push(function () {
            return am5.Bullet.new(root, {
                sprite: am5.Circle.new(root, {
                    strokeWidth: 3,
                    stroke: series3.get("stroke"),
                    radius: 5,
                    fill: root.interfaceColors.get("background")
                })
            });
        });

        var series4 = chart.series.push(
            am5xy.LineSeries.new(root, {
                name: "olevel",
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: "olevel",
                categoryXField: "response"
            })
        );

        series4.strokes.template.setAll({
            strokeWidth: 3,
            templateField: "strokeSettings"
        });


        series4.data.setAll(data);

        series4.bullets.push(function () {
            return am5.Bullet.new(root, {
                sprite: am5.Circle.new(root, {
                    strokeWidth: 3,
                    stroke: series4.get("stroke"),
                    radius: 5,
                    fill: root.interfaceColors.get("background")
                })
            });
        });

        var series5 = chart.series.push(
            am5xy.LineSeries.new(root, {
                name: "weather_condition",
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: "weather_condition",
                categoryXField: "response"
            })
        );

        series5.strokes.template.setAll({
            strokeWidth: 3,
            templateField: "strokeSettings"
        });


        series5.data.setAll(data);

        series5.bullets.push(function () {
            return am5.Bullet.new(root, {
                sprite: am5.Circle.new(root, {
                    strokeWidth: 3,
                    stroke: series5.get("stroke"),
                    radius: 5,
                    fill: root.interfaceColors.get("background")
                })
            });
        });

        chart.set("cursor", am5xy.XYCursor.new(root, {}));

        var legend = chart.children.push(
            am5.Legend.new(root, {
                centerX: am5.p50,
                x: am5.p50
            })
        );
        legend.data.setAll(chart.series.values);

        chart.appear(1000, 100);
        series1.appear();

    }); // end am5.ready()
}